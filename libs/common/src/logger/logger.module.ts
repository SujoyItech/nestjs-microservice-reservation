import { Module } from '@nestjs/common';
import { LoggerModule as PinoLoogerModule } from 'nestjs-pino';

@Module({
  imports: [
    PinoLoogerModule.forRoot({
      pinoHttp: {
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
          },
        },
      },
    }),
  ],
})
export class LoggerModule {}
